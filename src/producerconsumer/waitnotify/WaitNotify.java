package producerconsumer.waitnotify;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class WaitNotify {
	public static LinkedList<Integer> queue = new LinkedList<Integer>();
	public static final int MAX = 100;
	public static Logger log = Logger.getLogger("WaitNotify");
	public static void main(String[] args) {
		new WaitNotify();
	}
	WaitNotify(){
		ExecutorService service = Executors.newCachedThreadPool();
		for(int i=0;i<1;i++){
			service.submit(new Producer());
			service.submit(new Consumer());
		}
	}
	class Producer implements Runnable{

		@Override
		public void run() {
			while(true){
				synchronized (queue) {
					while(queue.size()>=MAX){
						try {
							log.info("Producer.wait()");
							queue.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					int item = (int) (Math.random() * Integer.MAX_VALUE - Integer.MAX_VALUE/3.0);
//					log.info("Produce Item: " + item + "\t\tNoch " + queue.size() + " Items in der Queue");
					queue.add(item);
					if(queue.size() == 1){
						log.info("Producer.notifyAll()");
						queue.notifyAll();
					}
				}
			}
		}
	}
	class Consumer implements Runnable{

		@Override
		public void run() {
			while(true){
				synchronized (queue) {
					while(queue.size()<=0){
						try {
							log.info("Consumer.wait()");
							queue.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					queue.removeFirst();
//					log.info("Consume Item: " + item + "\t\tNoch " + queue.size() + " Items in der Queue");
					if(queue.size() == MAX-1){
						log.info("Consumer.notifyAll()");
						queue.notifyAll();
					}
				}
			}
		}
	}
}
