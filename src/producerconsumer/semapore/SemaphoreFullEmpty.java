package producerconsumer.semapore;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SemaphoreFullEmpty {
	public static Logger log = Logger.getLogger("SemaphoreFullEmpty");
	public static final int MAX = 10000;
	public static Semaphore mutex;
	public static Semaphore empty;
	public static Semaphore full;
	public static LinkedList<Integer> queue;

	public static void main(String[] arg) {
		mutex = new Semaphore(1);
		empty = new Semaphore(MAX);
		try {
			// empty hat MAX "verbraucher" => Warteschlange ist leer, es können keine Consumer gestartet werden.
			empty.acquire(MAX); 		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// full hat keine "verbraucher" => Die Warteschlange ist nicht voll, es können Producer gestartet werden.
		full = new Semaphore(MAX); 
		queue = new LinkedList<Integer>();
		new SemaphoreFullEmpty();
	}

	public SemaphoreFullEmpty() {
		ExecutorService service = Executors.newCachedThreadPool();
		for (int i = 0; i < 1; i++) {
			service.submit(new Producer());
			service.submit(new Consumer());
		}
	}

	class Producer implements Runnable {
		@Override
		public void run() {
			while (true) {
				try {
					boolean m = mutex.tryAcquire(5, TimeUnit.MILLISECONDS);
					if (m) {
						// Verbraucher für full => wenn true dann ist die Wareschlange nicht voll.
						boolean e = full.tryAcquire(5, TimeUnit.MILLISECONDS);
						if (e) {
							int item = (int) (Math.random() * Integer.MAX_VALUE - Integer.MAX_VALUE / 3.0);
							queue.add(item);
							// Gibt einen Verbraucher für empty frei => es können Consumer gestartet werden.
							empty.release();
							log.info("Produce Item: " + item + "\t\tNoch "
									+ queue.size() + " Items in der Queue\t\t" + empty.availablePermits());
						}
						mutex.release();
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	class Consumer implements Runnable {
		@Override
		public void run() {
			while (true) {
				try {
					boolean m = mutex.tryAcquire(5, TimeUnit.MILLISECONDS);
					if (m) {
						// Verbraucher für empty => wenn true dann ist die Wareschlange nicht leer.
						boolean e = empty.tryAcquire(5, TimeUnit.MILLISECONDS);
						if (e) {
							int item = queue.removeFirst();
							log.info("Consumer Item: " + item + "\t\tNoch "
									+ queue.size() + " Items in der Queue\t\t" + empty.availablePermits());
							// Gibt einen Verbraucher für full frei => es können Producer gestartet werden.
							full.release();
						}
						mutex.release();
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
