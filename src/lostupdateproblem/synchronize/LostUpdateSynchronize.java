package lostupdateproblem.synchronize;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class LostUpdateSynchronize implements Runnable {
	public static Object synchronize = new Object();
	public static int wert;

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			synchronized (synchronize) {
				int tmp = wert;
				tmp++;
				wert = tmp;
			}
		}
	}

	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		try {
			Future<?> oneThread = service.submit(new LostUpdateSynchronize());
			oneThread.get();
			System.out.println("Globaler Wert mit einem Thread: " + wert);
			List<Future<?>> future;
			for (int i = 2; i <= 500; i++) {
				wert = 0;
				future = new ArrayList<Future<?>>();
				for (int n = 0; n < i; n++) {
					future.add(service.submit(new LostUpdateSynchronize()));
				}
				for (Future<?> f : future) {
					f.get();
				}
				System.out.println("Globaler Wert mit " + i + " Threads: "
						+ wert + "\terwartet: " + (i * 10));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
