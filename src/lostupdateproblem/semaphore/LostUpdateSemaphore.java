package lostupdateproblem.semaphore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class LostUpdateSemaphore implements Runnable {
	public static Semaphore semaphore = new Semaphore(1);
	public static int wert;

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			boolean acquired;
			try {
				while(true){
					acquired = semaphore.tryAcquire(800, TimeUnit.MILLISECONDS);
					if(acquired){
						int tmp = wert;
						tmp++;
						wert = tmp;
						semaphore.release();
						break;
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		try {
			Future<?> oneThread = service.submit(new LostUpdateSemaphore());
			oneThread.get();
			System.out.println("Globaler Wert mit einem Thread: " + wert);
			List<Future<?>> future;
			for (int i = 2; i <= 500; i++) {
				wert = 0;
				future = new ArrayList<Future<?>>();
				for (int n = 0; n < i; n++) {
					future.add(service.submit(new LostUpdateSemaphore()));
				}
				for (Future<?> f : future) {
					f.get();
				}
				System.out.println("Globaler Wert mit " + i + " Threads: "
						+ wert + "\terwartet: " + (i * 10));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
