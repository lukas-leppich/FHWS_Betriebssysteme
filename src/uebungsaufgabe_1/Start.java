package uebungsaufgabe_1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

public class Start {
	public static Logger logger = Logger.getLogger("Start");
	public static void main(String[] args) {
		String file = "bild.jpg";
		if(args.length >= 1){
			file = args[0];
		}
		try {
			long start = System.nanoTime();
			ExecutorService service = Executors.newFixedThreadPool(4);
			@SuppressWarnings("rawtypes")
			List<Future> futures = new ArrayList<Future>();
			BufferedImage bimg = ImageIO.read(new File(file));
			for(int i=0;i<bimg.getHeight();i++){
				futures.add(service.submit(new Worker(bimg, i)));
			}
			for(@SuppressWarnings("rawtypes") Future f : futures){
				f.get();
			}
			service.shutdown();
			ImageIO.write(bimg, "jpeg", new File("output.jpg"));
			logger.info("Verbrauchte Zeit: " + (System.nanoTime() - start));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
