package uebungsaufgabe_1;

import java.awt.image.BufferedImage;
import java.util.logging.Logger;

public class Worker implements Runnable {
	public static Logger logger = Logger.getLogger("Worker");
	private BufferedImage bimg;
	private int height;

	public Worker(BufferedImage image, int height) {
		logger.info("Init Worker für Zeile " + height);
		this.bimg = image;
		this.height = height;
	}
	@Override
	public void run() {
		for (int i = 0; i < bimg.getWidth(); i++) {
			int wert = bimg.getRGB(i, height);
			wert = wert ^ 0xFFFFFF;
			bimg.setRGB(i, height, wert);
		}
	}
}
