package uebungsaufgabe_2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

public class Start {
	public static Logger logger = Logger.getLogger("Start");
	public static void main(String[] args) {
		int[] feld = new int[0xFFFFFF];
		List<Future<Integer>> future;
		List<Integer> maxvalues;
		ExecutorService service = Executors.newCachedThreadPool();
		for(int i=0;i<feld.length;i++){
			feld[i] = (int) ((Math.random() * Integer.MAX_VALUE) - Integer.MAX_VALUE/2);
		}
		try {
			for(int N = 1; N <= 8; N++){
				long gesamtTime = 0;
				int prevMax = 0;
				int anzahlVersuche = 100;
				for(int e=0;e<anzahlVersuche;e++){
					int max = 0;
					long time = System.nanoTime();
					if(N == 1){
						Worker w = new Worker(feld, 0, feld.length);
						max = w.call();
					} else{
						maxvalues = new ArrayList<Integer>();
						future = new ArrayList<Future<Integer>>();
						int m = feld.length;
						int teil = m / N;
						for(int i=0;i<N;i++){
							future.add(service.submit(new Worker(feld, teil*i + (i>0?1:0), teil*(i+1))));
						}
						if (m%N != 0){
							maxvalues.add(feld[feld.length-1]);
						}
						for(Future<Integer> f : future){
							maxvalues.add(f.get());
						}
						Worker w = new Worker(maxvalues, 0, maxvalues.size());
						max = w.call();
					}
					if(prevMax == 0){
						prevMax = max; 
					} else{
						if(max != prevMax){
							logger.info("Unterschiedliche Max Werte: Alt(" + prevMax+") Neu("+max+")");
						}
					}
					gesamtTime += System.nanoTime() - time;
				}
				System.out.println("Mit "+(N==0?1:N)+" Threads: Max = " + prevMax + " Zeit = " + (gesamtTime/anzahlVersuche));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		service.shutdown();
	}
}
