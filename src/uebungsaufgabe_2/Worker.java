package uebungsaufgabe_2;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class Worker implements Callable<Integer> {
	public static Logger logger = Logger.getLogger("Worker");
	private int start, ende;
	private int[] array = null;
	private List<Integer> liste = null;
	public Worker(final int[] array, int start, int ende){
		this.start = start;
		this.ende = ende;
		this.array = array;
	}
	public Worker(final List<Integer> liste, int start, int ende){
		this.start = start;
		this.ende = ende;
		this.liste = liste;
	}
	@Override
	public Integer call() throws Exception {
		int max = Integer.MIN_VALUE;
		if(this.array != null){
			for(int i=start;i < ende; i++) {
				max = Math.max(max, array[i]);
			}
		} else {
			for(int i=start;i < ende; i++) {
				max = Math.max(max, liste.get(i));
			}
		}
		return max;
	}

}
